# language: es

Característica: Google Translate
  como usaurio web
  quiero usar google translate
  para traducir palabras en diferentes lenguajes

  Escenario: Traduccion Susana de inglés a español
    Dado Susana quiere traducir una palabra
    Cuando ella traduce la palabra cheese de inglés a español
    Entonces ella debe ver la palabra queso

  Escenario: Traduccion de español a ingles
    Dado Orlando quiere traducir una palabra
    Cuando el traduce la palabra casa de español a inglés
    Entonces ella debe ver la palabra house

  Escenario: Traduccion de japones a español
    Dado daniela quiere traducir una palabra
    Cuando ella traduce la palabra はじめまして de Japonés a español
    Entonces ella debe ver la palabra bueno
