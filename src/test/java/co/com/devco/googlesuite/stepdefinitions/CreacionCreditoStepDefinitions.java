package co.com.devco.googlesuite.stepdefinitions;

import co.com.devco.googlesuite.question.VerificacionCreacion;
import co.com.devco.googlesuite.question.VerificarIngreso;
import co.com.devco.googlesuite.task.Comercial;
import co.com.devco.googlesuite.task.CreandoInfoCredito;
import co.com.devco.googlesuite.task.Ingreso;
import co.com.devco.googlesuite.task.Sesion;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Click;


import static co.com.devco.googlesuite.userinterfaces.CreacionComponent.COMERCIAL;
import static co.com.devco.googlesuite.userinterfaces.CreacionComponent.NUEVOCREDITO;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;
import static org.hamcrest.Matchers.equalTo;

public class CreacionCreditoStepDefinitions {


    @Dado("que (.*) quiere crear un credito")
    public void queQuiereCrearUnCredito(String nombreActor) {

        theActorCalled(nombreActor).attemptsTo(
                Sesion.sesion(),
                Ingreso.usuario("ochinchilla").de("Nicolas32@").a("123"),
                Click.on(COMERCIAL),
                Click.on(NUEVOCREDITO));
    }

    @Cuando("el ingresa la cedula (.*) el valor (.*) y el plazo (.*)")
    public void elIngresaLaCedulaElValorYElPlazo(String cedula, String cuota, String plazo) {
        theActorInTheSpotlight().attemptsTo(CreandoInfoCredito.cedula(cedula).de(cuota).a(plazo));

    }

    @Entonces("el debería crear un credito")
    public void elDeberíaCrearUnCredito() {

    }


}
