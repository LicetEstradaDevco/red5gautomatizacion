package co.com.devco.googlesuite.stepdefinitions;


import co.com.devco.googlesuite.task.*;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.devco.googlesuite.userinterfaces.CreacionComponent.COMERCIAL;
import static co.com.devco.googlesuite.userinterfaces.FormularioSolicitudComponent.FORMLIST;
import static co.com.devco.googlesuite.userinterfaces.FormularioSolicitudComponent.MIS_CREDITOS;

import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class FormularioSolicitudStepDeffinitions {

    @Dado("que (.*) quiere llenar el formulario de solicitud")
    public void queQuiereLlenarElFormularioDeSolicitud(String nombreActor) {
        theActorCalled(nombreActor).attemptsTo(
                Sesion.sesion(),
                Ingreso.usuario("ochinchilla").de("Nicolas32@").a("123"),
                Click.on(COMERCIAL),
                Click.on(MIS_CREDITOS),
                WaitUntil.the(FORMLIST,isCurrentlyEnabled()),
                Click.on(FORMLIST)
        );

    }

    @Cuando("el ingresa al modulo de comercial llena todo los 8 pasos")
    public void elIngresaAlModuloDeComercialDebeIngresarAlModuloDeMisCreditos() {

        FormSolicitudPaso1 formSol1 = new FormSolicitudPaso1();
        FormSolicitudPaso2 formSol2 = new FormSolicitudPaso2();
        FormSolicitudPaso3 formSol3 = new FormSolicitudPaso3();
        FormSolicitudPaso4 formSol4 = new FormSolicitudPaso4();
        FormSolicitudPaso5 formSol5 = new FormSolicitudPaso5();



        FormSolicitudPaso6 formSol6 = new FormSolicitudPaso6();
        FormSolicitudPaso7 formSol7 = new FormSolicitudPaso7();
        FormSolicitudPaso8 formSol8 = new FormSolicitudPaso8();

        theActorInTheSpotlight().attemptsTo(formSol1,formSol2,formSol3,formSol4,formSol5,formSol6,formSol7,formSol8);

    }

    @Entonces("el debería ver  el credito")
    public void elDeberíaVerElCredito() {


    }
}
