package co.com.devco.googlesuite.interacion;

import co.com.devco.googlesuite.task.FormSolicitudPaso1;
import co.com.devco.googlesuite.task.FormSolicitudPaso2;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;



import static co.com.devco.googlesuite.userinterfaces.FormularioSolicitudComponent.TASAMAXLEGVIG;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

