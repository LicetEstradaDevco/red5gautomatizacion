package co.com.devco.googlesuite.userinterfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class FormularioSolicitudComponent {

    public static final Target MIS_CREDITOS =  Target.the("Imput de cedula").located(By.id("mis_creditos"));

    public static final Target CEDULAFS =  Target.the("Imput de cedula").located(By.xpath("//*[@id=\"searchDoc\"]"));
    public static final Target SOLICITUD =  Target.the("Imput de solicitud").located(By.xpath("//*[@id=\"searchEst\"]"));
    public static final Target SELECCIONAR =  Target.the("Imput de solicitud").located(By.xpath("//*[@id=\"searchEst\"]"));


    public static final Target FORMLIST =  Target.the("Imput de solicitud").located(By.xpath("//*[@id=\"misCreditos\"]/table/tbody/tr[1]/td[4]/a"));




    /* PASO 1  */


    public static final Target CIUDAD =  Target.the("Imput de solicitud").located(By.xpath("//*[@id=\"step-1\"]/div[2]/div[2]/div[4]/div/button"));
    public static final Target CIUDADCAMPO =  Target.the("Imput de solicitud").located(By.xpath("//*[@id=\"step-1\"]/div[2]/div[2]/div[4]/div/div/div/input"));
    public static final Target CALIFICACION =  Target.the("Imput de calificacion").located(By.id("txtCalf"));
    public static final Target VINCULACION =  Target.the("Imput de vinculacion").located(By.xpath("//*[@id=\"step-1\"]/div[2]/div[3]/div[4]/div/button"));
    public static final Target VINCULACION2 =  Target.the("Imput de vinculacion2").located(By.xpath("//*[@id=\"step-1\"]/div[2]/div[3]/div[4]/div/div/ul/li[2]/a/span[1]"));
    public static final Target MONTO =  Target.the("Imput de monto").located(By.id("txtMonto"));
    public static final Target CUOTA2 =  Target.the("Imput de cuota2").located(By.id("cuota"));
    public static final Target PLAZO =  Target.the("Imput de plazo").located(By.id("txtPlaz"));
    public static final Target TASAINT =  Target.the("Imput de tasa interes").located(By.id("tasaI"));
    public static final Target TASAMOR =  Target.the("Imput de tasa mora").located(By.id("tasaM"));
    public static final Target TASAMAXLEGVIG =  Target.the("Imput de tasa maxima").located(By.id("tasaMax"));

    public static final Target CONTINUAR1 =  Target.the("boton guardar y continuar").located(By.id("btnEnviar_1"));

    /* PASO 2  */

    /*APELLIDO*/
    public static final Target APELLIDO =  Target.the("Imput de apellido").located(By.id("txtPApellido"));
    /*NOMBRE*/
    public static final Target NOMBRE =  Target.the("Imput de nombre").located(By.id("txtPNombre"));


    /*SELECCION DE CEDULA*/
    public static final Target SELCEDULA =  Target.the("Imput de cedula").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[2]/div[4]/div/button"));
    public static final Target SELCEDULA2 =  Target.the("Imput de cedula2").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[2]/div[4]/div/div/ul/li[2]/a"));


    /*SELECCION DE LUGAR LUGEXPEDICION*/
    public static final Target LUGEXPEDICION =  Target.the("Imput de expedicio").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[3]/div[2]/div/button"));
    public static final Target LUGEXPEDICION2 =  Target.the("Imput de expedicio2").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[3]/div[2]/div/div/ul/li[2]/a"));

    /*FECHA DE EXP*/
    public static final Target FECHEXPEDICION =  Target.the("Imput de expedicio").located(By.xpath("//*[@id=\"txtFechaExp\"]"));
    public static final Target FECHEXPEDICION2 =  Target.the("Imput de expedicio2").located(By.xpath("/html/body/div[38]/div[1]/table/tbody/tr[3]/td[3]"));

    /*FECHA NACIMIENTO*/
    public static final Target FECHANACIMIENTO =  Target.the("Imput de fechanacimiento").located(By.xpath("//*[@id=\"txtFechaNac\"]"));
    public static final Target FECHANACIMIENTO2 =  Target.the("Imput de fechanacimiento2").located(By.xpath("/html/body/div[38]/div[1]/table/tbody/tr[4]/td[2]"));

    /*SEXO*/
    public static final Target ESTADO =  Target.the("Imput de estado civil").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[4]/div[2]/div/button"));
    public static final Target ESTADO2 =  Target.the("Imput de estado civil").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[4]/div[2]/div/div/ul/li[2]/a"));

    /*LUGAR DE NACIMIENTO*/
    public static final Target LUGNACIMIENTO =  Target.the("Imput de lugar nacimiento").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[4]/div[4]/div/button"));
    public static final Target LUGNACIMIENTO2 =  Target.the("Imput de lugar nacimiento 2").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[4]/div[4]/div/div/ul/li[2]/a/span[1]"));

    /*ESTADO CIVIL*/
    public static final Target ESTADOCIVIL =  Target.the("Imput de estado civil").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[4]/div[6]/div/button"));
    public static final Target ESTADOCIVIL2 =  Target.the("Imput de estado civil2").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[4]/div[6]/div/div/ul/li[2]/a/span[1]"));


    /*CIUDADCAMPO Y DEPARTAMENTO*/
    public static final Target CIUDADEP =  Target.the("Imput de ciudad y departamento").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[5]/div[4]/div/button"));
    public static final Target CIUDADEP2 =  Target.the("Imput de ciudad y departamento2").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[5]/div[4]/div/div/ul/li[126]/a/span[1]"));


    /*TIPO DE VIVIENDA*/
    public static final Target TIPOVIVIENDA =  Target.the("Imput de tipo de vivienda").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[5]/div[6]/div/button"));
    public static final Target TIPOVIVIENDA2 =  Target.the("Imput de tipo de viviend2").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[5]/div[6]/div/div/ul/li[2]"));





    /*ESTRATO*/
    public static final Target ESTRATO =  Target.the("Imput de tipo estrato").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[6]/div[2]/div/button"));
    public static final Target ESTRATO2 =  Target.the("Imput de tipo estato2").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[6]/div[2]/div/div/ul/li[4]/a"));

    /*DIRECCION BOTON ACEPTAR*/
    public static final Target DIRECCION =  Target.the("Imput de direccion").located(By.id("dirCa"));
    public static final Target BOTONDIR =  Target.the("boton de direccion").located(By.xpath("//*[@id=\"dvDirecciones\"]/div/div/div[3]/button[2]"));

    /*BARRIO*/
    public static final Target BARRIO =  Target.the("Imput de barrio").located(By.id("txtBarrio"));

    /*TELEFONO*/
    public static final Target TELEFONO =  Target.the("Imput de telefono").located(By.id("txtTel"));


    /*CELULAR*/
    public static final Target CELULAR =  Target.the("Imput de celular").located(By.id("txtCel"));


    /*CORRESPONSAL*/
    public static final Target CORRESPONSAL =  Target.the("Imput de corresponsal").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[8]/div[4]/div/button"));
    public static final Target CORRESPONSAL2 =  Target.the("boton de corresponsal2").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[8]/div[4]/div/div/ul/li[2]/a"));

    /*EMAIL*/
    public static final Target EMAIL =  Target.the("Imput de corresponsal").located(By.id("txtEmail"));

    /*AÑOS RESIDENCIA Y MESES*/

    public static final Target ANOSRESIDENCIA =  Target.the("Imput de corresponsal").located(By.id("txtAnios"));
    public static final Target MESESRESIDENCIA =  Target.the("Imput de corresponsal").located(By.id("txtMeses"));

    /*EPS*/

    public static final Target EPS =  Target.the("Imput de corresponsal").located(By.id("txtEps"));



    /*ADULTOS Y NIÑOS*/

    public static final Target ADULTO =  Target.the("Imput de corresponsal").located(By.id("txtAdulto"));
    public static final Target NINO =  Target.the("Imput de corresponsal").located(By.id("txtMenor18"));

    /*NIVEL DE ESTUDIO*/

    public static final Target NIVEL =  Target.the("Imput de nivel").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[11]/div[2]/div/button"));
    public static final Target NIVEL2 =  Target.the("Imput de nivel2").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[11]/div[2]/div/div/ul/li[5]/a/span[1]"));

    /*PROFESION*/

    public static final Target PROFESION =  Target.the("Imput de profesion").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[11]/div[4]/div/button"));
    public static final Target PROFESION2 =  Target.the("Imput de profesion").located(By.xpath("//*[@id=\"step-2\"]/div[2]/div[11]/div[4]/div/div/ul/li[2]/a"));

    /*BOTON ACEPTAR*/

    public static final Target BOTONACEPTAR2 =  Target.the("Imput de corresponsal").located(By.id("btnEnviar_2"));







    /* PASO 3  */



    /* EMPRESA TRABAJO */
    public static final Target EMPRESATRAB =  Target.the("Imput de empresa").located(By.xpath("//*[@id=\"step-3\"]/div[2]/div[7]/div[2]/div/button"));
    public static final Target EMPRESATRAB2 =  Target.the("Imput de empresa2").located(By.xpath("//*[@id=\"step-3\"]/div[2]/div[7]/div[2]/div/div/ul/li[64]/a/span[1]"));

    /* CARGO*/
    public static final Target CARGO =  Target.the("Imput de corresponsal").located(By.id("txtCargo"));


    /* FECHA DE VINCULACION*/
    public static final Target FECHAVINCULACION =  Target.the("Imput de fechavinculacion").located(By.xpath("//*[@id=\"txtFechaVinculacion\"]"));
    public static final Target FECHAVINCULACION2 =  Target.the("Imput de fechavinculacion").located(By.xpath("/html/body/div[38]/div[1]/table/tbody/tr[4]/td[2]"));

    /* DIRECCION TRABAJO Y BOTON*/
    public static final Target DIRTRAB =  Target.the("Imput de direccion trabajo").located(By.id("dirTrabajo"));
    public static final Target BOTONDIRTRAB =  Target.the("Imput de direccion trabajo").located(By.xpath("//*[@id=\"dvDirecciones\"]/div/div/div[3]/button[2]"));

    /*CIUDADCAMPO DE TRABAJO*/
    public static final Target CIUDADTRABAJO =  Target.the("Imput de ciudad de trabajo").located(By.xpath("//*[@id=\"step-3\"]/div[2]/div[8]/div[4]/div/button"));
    public static final Target CIUDADTRABAJO2 =  Target.the("Imput de ciudad de trabajo").located(By.xpath("//*[@id=\"step-3\"]/div[2]/div[8]/div[4]/div/div/ul/li[126]/a/span[1]"));

    /* TELEFONO DE TRABAJO*/
    public static final Target TELEFONOTRAB =  Target.the("Imput telefono trabajo").located(By.id("txtTelEmpresa"));

    /* EXTENCION*/
    public static final Target EXT =  Target.the("Imput de extension").located(By.id("txtExt"));

    /*BOTON ACEPTAR*/
    public static final Target BOTONACEPTAR3 =  Target.the("Imput de boton").located(By.id("btnEnviar_3"));




    /* PASO 4  */



    /* ACTIVOS CORRIENTES */
    public static final Target ACTCORRIENTE =  Target.the("Imput de corriente").located(By.id("activos_corrientes"));

    /* PASIVOS FINANCIEROS*/
    public static final Target PASFINANCIERO =  Target.the("Imput de pasivo").located(By.id("pasivos_financieros"));

    /* ACTIVOS*/
    public static final Target ACTIVOS =  Target.the("Imput de activos").located(By.id("activos_fijos"));

    /* PASIVOS CORRIENTE*/
    public static final Target PASCORRIENTE =  Target.the("Imput de pasivocor").located(By.id("pasivos_corrientes"));

    /* OTROS ACTIVOS*/
    public static final Target OTROSACT =  Target.the("Imput de pasivocor").located(By.id("otros_activos"));

    /* OTROS PASIVOS*/
    public static final Target OTROSPAS =  Target.the("Imput de pasivocor").located(By.id("otros_pasivos"));



    /* PASO 4  DESCRIPCION DE LOS ACTIVOS*/

    /*TIPO INMUEBLE*/
    public static final Target TIPINMUEBLE =  Target.the("Imput de inmueble").located(By.id("txtTipoInm"));

    /*VALOR COMERCIAL*/
    public static final Target VALCOMER =  Target.the("Imput de corresponsal").located(By.id("txtValorComInm"));

    /*HIPOTECADO*/
    public static final Target HIPOTECADO =  Target.the("Imput de corresponsal").located(By.id("txtHipotecado"));

    /*DIRECCION BIENES*/
    public static final Target DIRECCIONBIENES =  Target.the("Imput de direccion bienes").located(By.id("dirBienes"));
    public static final Target BOTONDIRBIENES =  Target.the("Imput de boton direccion").located(By.xpath("//*[@id=\"dvDirecciones\"]/div/div/div[3]/button[2]"));


    /*SALDO CREDITO*/
    public static final Target SALDOCREDITO =  Target.the("Imput de saldo credito").located(By.id("txtSaldoCredInm"));

    /*VEHICULO*/
    public static final Target VEHICULO =  Target.the("Imput de vehiculo").located(By.id("txtVehCls"));

    /*COMERCIAL*/
    public static final Target COMERCIAL =  Target.the("Imput de saldo comercial").located(By.id("txtValorComVeh"));

    /*COMERCIAL*/
    public static final Target MARCA =  Target.the("Imput de marca").located(By.id("txtMarcaVeh"));

    /*PLACA*/
    public static final Target PLACA =  Target.the("Imput de marca").located(By.id("txtPlacaVeh"));

    /*SALDO*/
    public static final Target SALDO =  Target.the("Imput de marca").located(By.id("txtSaldoCredVeh"));


    /*PRENDA*/
    public static final Target PRENDA =  Target.the("Imput de marca").located(By.id("txtPrendaFavVeh"));

    /*USAR DINERO*/

    public static final Target USARDINERO =  Target.the("Imput de boton direccion").located(By.xpath("//*[@id=\"step-4\"]/div[4]/div[2]/div[2]/div/button"));
    public static final Target USARDINERO2 =  Target.the("Imput de boton direccion").located(By.xpath("//*[@id=\"step-4\"]/div[4]/div[2]/div[2]/div/div/ul/li[5]/a"));


    /*BOTON ACEPTAR*/
    public static final Target BOTONACEPTAR4 =  Target.the("boton envio 4").located(By.id("btnEnviar_4"));



    /* PASO 5  */


    /*BOTON ACEPTAR*/
    public static final Target BOTONACEPTAR5 =  Target.the("Imput de corresponsal").located(By.id("btnEnviar_5"));



    /* PASO 6 */

    /*NOMBRE REFERENCIA FAMILIAR*/
    public static final Target NOMREFERENCIF =  Target.the("Imput de referencia").located(By.id("ref_familiar_nombre"));

    /*PARENTESCO*/
    public static final Target REFERENCIAPARENTESCOF =  Target.the("Imput de referencia").located(By.id("ref_familiar_par"));


    /*TELEFONO FIJO*/
    public static final Target REFERENCIATEL =  Target.the("Imput de referencia").located(By.id("ref_familiar_tel"));

    /*CIUDADCAMPO */
    public static final Target  CIUDADREFERENCIA =  Target.the("Imput de referencia").located(By.xpath("//*[@id=\"step-6\"]/div[5]/div[2]/div[2]/div/button"));
    public static final Target  CIUDADREFERENCIA2 =  Target.the("Imput de referencia").located(By.xpath("//*[@id=\"step-6\"]/div[5]/div[2]/div[2]/div/div/ul/li[126]/a/span[1]"));

    /*CELULAR*/
    public static final Target REFCELULAR =  Target.the("Imput de referencia").located(By.id("txtCelRFam"));

    /*DIRECCION Y BOTTON*/
    public static final Target REFDIRECCION =  Target.the("Imput de referencia").located(By.id("dirFamiliar"));
    public static final Target  REFDIRECCION2 =  Target.the("Imput de referencia").located(By.xpath("//*[@id=\"dvDirecciones\"]/div/div/div[3]/button[2]"));




    /*NOMBRE REFERENCIA PERSONAL*/

    public static final Target NOMREFERENCIP =  Target.the("Imput de referencia").located(By.id("ref_personal_nombre"));

    /*NOMBRE REFERENCIA PERSONAL*/
    public static final Target REFERENCIAPARENTESCOP2 =  Target.the("Imput de referencia").located(By.id("ref_personal_par"));

    /*PARENTESCO PERSONAL*/
    public static final Target REFERENCIATELPERSONAL =  Target.the("Imput de referencia").located(By.id("ref_personal_tel"));

    /*CIUDADCAMPO PERSONAL*/
    public static final Target CIUDADPERSONAL =  Target.the("Imput de referencia").located(By.xpath("//*[@id=\"step-6\"]/div[9]/div[2]/div[2]/div/button"));
    public static final Target CIUDADPERSONAL2 =  Target.the("Imput de referencia").located(By.xpath("//*[@id=\"step-6\"]/div[9]/div[2]/div[2]/div/div/ul/li[126]/a/span[1]"));



    /*CELUALR PERSONAL*/
    public static final Target CELULARPERSONAL =  Target.the("Imput de referencia").located(By.id("txtCelRPer"));

    /*DIRECCION PERSONAL Y BOTON*/
    public static final Target DIRECCIONPERSONAL =  Target.the("Imput de referencia").located(By.id("dirPersonal"));
    public static final Target DIRECCIONPERSONAL2 =  Target.the("Imput de referencia").located(By.xpath("//*[@id=\"dvDirecciones\"]/div/div/div[3]/button[2]"));





    /*ENTIDAD FINANCIERA*/
    public static final Target ENTIDADFINANCIERA =  Target.the("Imput de referencia").located(By.id("txtNEntFin"));


    /*SUCURSAL*/
    public static final Target SUCURSAL =  Target.the("Imput de referencia").located(By.id("txtSucursal"));

    /*TIPO DE PRODUCTO*/
    public static final Target TIPOPROD =  Target.the("Imput de referencia").located(By.xpath("//*[@id=\"step-6\"]/div[13]/div/div[6]/div/button"));
    public static final Target TIPOPROD2 =  Target.the("Imput de referencia").located(By.xpath("//*[@id=\"step-6\"]/div[13]/div/div[ 6]/div/div/ul/li[2]/a"));


    /*BOTON ACEPTAR*/
    public static final Target BOTONACEPTAR6 =  Target.the("Imput de corresponsal").located(By.id("btnEnviar_6"));






    /* PASO 7*/

    /*BOTON ACEPTAR*/
    public static final Target BOTONACEPTAR7 =  Target.the("Imput de corresponsal").located(By.id("btnEnviar_7"));

    /* PASO 8*/

    public static final Target BOTONACEPTAR8 =  Target.the("Imput de corresponsal").located(By.xpath("//*[@id=\"step-8\"]/div[6]/div/button[3]"));

}
