package co.com.devco.googlesuite.userinterfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class CreacionComponent {
    public static final Target COMERCIAL =  Target.the("Boton comercial").located(By.id("comercial"));
    public static final Target NUEVOCREDITO =  Target.the("Boton nuevo_credito").located(By.id("nuevo_credito"));
    public static final Target CEDULA =  Target.the("Campo de cedula").located(By.id("textoDoc"));
    public static final Target CELULAR =  Target.the("Campo de numero").located(By.id("textoTel"));
    public static final Target INFO =  Target.the("Campo de numero").located(By.id("tablaInfo"));


    public static final Target CORREO =  Target.the("Campo de correo").located(By.id("textoEmail"));
    public static final Target CUOTA =  Target.the("Campo de cuota").located(By.id("textoVlrCuota"));
    public static final Target PLAZO =  Target.the("Campo de plazo").located(By.id("textoNumCuotas"));
    public static final Target CONFIRMAR =  Target.the("Campo de plazo").located(By.xpath("/html/body/div[40]"));

    public static final Target CREAR =  Target.the("Boton de crear").located(By.id("btn_Dif"));
    public static final Target CUENTA2= Target.the("Boton cuenta").located (By.xpath("/html/body/header/div[2]/div/div[2]/div/p"));

}
