package co.com.devco.googlesuite.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.devco.googlesuite.userinterfaces.CreacionComponent.COMERCIAL;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class Comercial implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
        WaitUntil.the(COMERCIAL,isCurrentlyEnabled()),
        Click.on(COMERCIAL)
        );

    }
}
