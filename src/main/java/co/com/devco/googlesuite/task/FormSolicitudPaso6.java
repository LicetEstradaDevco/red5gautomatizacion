package co.com.devco.googlesuite.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.devco.googlesuite.userinterfaces.FormularioSolicitudComponent.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class FormSolicitudPaso6  implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(


                WaitUntil.the(NOMREFERENCIF,isCurrentlyEnabled()),
                Click.on(NOMREFERENCIF),
                Enter.theValue("Carlos Martinez Ruiz").into(NOMREFERENCIF),

                Click.on(REFERENCIAPARENTESCOF),
                Enter.theValue("Hermano").into(REFERENCIAPARENTESCOF),

                Click.on(REFERENCIATEL),
                Enter.theValue("3508966").into(REFERENCIATEL),


                Click.on(CIUDADREFERENCIA),
                Click.on(CIUDADREFERENCIA2),

                Click.on(REFCELULAR),
                Enter.theValue("3114589966").into(REFCELULAR),

                Click.on(REFDIRECCION),
                Enter.theValue("CRA 73 No. 20-45").into(REFDIRECCION),
                Click.on(REFDIRECCION2),


                // PERSONAL


                WaitUntil.the(NOMREFERENCIP,isCurrentlyEnabled()),
                Click.on(NOMREFERENCIP),
                Enter.theValue("Luis Rodriguez Montes").into(NOMREFERENCIP),

                Click.on(REFERENCIAPARENTESCOP2),
                Enter.theValue("AMIGO").into(REFERENCIAPARENTESCOP2),

                Click.on(REFERENCIATELPERSONAL),
                Enter.theValue("3836699").into(REFERENCIATELPERSONAL),


                Click.on(CIUDADPERSONAL),
                Click.on(CIUDADPERSONAL2),

                Click.on(CELULARPERSONAL),
                Enter.theValue("3184996633").into(CELULARPERSONAL),

                Click.on(DIRECCIONPERSONAL),
                Enter.theValue("Cll 90 no 55-39").into(DIRECCIONPERSONAL),
                Click.on(DIRECCIONPERSONAL2),


                Click.on(ENTIDADFINANCIERA),
                Enter.theValue("Bancomeva").into(ENTIDADFINANCIERA),
                Click.on(ENTIDADFINANCIERA),

                Click.on(SUCURSAL),
                Enter.theValue("Centro").into(SUCURSAL),
                Click.on(SUCURSAL),

                Click.on(TIPOPROD),
                Click.on(TIPOPROD2),

                Click.on(BOTONACEPTAR6)


        );
    }
}
