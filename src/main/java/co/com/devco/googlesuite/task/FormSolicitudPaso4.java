package co.com.devco.googlesuite.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;


import static co.com.devco.googlesuite.userinterfaces.FormularioSolicitudComponent.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class FormSolicitudPaso4 implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                WaitUntil.the(ACTCORRIENTE,isCurrentlyEnabled()),
                Click.on(ACTCORRIENTE),
                Enter.theValue("1000000").into(ACTCORRIENTE),
                Click.on(PASFINANCIERO),
                Enter.theValue("300000").into(PASFINANCIERO),
                Click.on(ACTIVOS),
                Enter.theValue("4000000").into(ACTIVOS),
                Click.on(PASCORRIENTE),
                Enter.theValue("4000000").into(PASCORRIENTE),
                Click.on(OTROSACT),
                Enter.theValue("800000").into(OTROSACT),

                Click.on(OTROSPAS),
                Enter.theValue("100000").into(OTROSPAS),

                Click.on(TIPINMUEBLE),
                Enter.theValue("3000000").into(TIPINMUEBLE),

                Click.on(VALCOMER),
                Enter.theValue("23000000").into(VALCOMER),

                Click.on(HIPOTECADO),
                Enter.theValue("0").into(HIPOTECADO),

                Click.on(DIRECCIONBIENES),
                Enter.theValue("CRA 73 No. 20-45").into(DIRECCIONBIENES),
                Click.on(BOTONDIRBIENES),

                Click.on(SALDOCREDITO),
                Enter.theValue("3000000").into(SALDOCREDITO),

                Click.on(VEHICULO),
                Enter.theValue("SEDAN").into(VEHICULO),

                Click.on(COMERCIAL),
                Enter.theValue("31000000").into(COMERCIAL),

                Click.on(MARCA),
                Enter.theValue("MAZDA 3").into(MARCA),

                Click.on(PLACA),
                Enter.theValue("HUY-300").into(PLACA),

                Click.on(SALDO),
                Enter.theValue("0").into(SALDO),

                Click.on(PRENDA),
                Enter.theValue("0").into(PRENDA),

                Click.on(USARDINERO),
                Click.on(USARDINERO2),

                WaitUntil.the(BOTONACEPTAR4,isCurrentlyEnabled()),
                Click.on(BOTONACEPTAR4)


        );
    }
}
