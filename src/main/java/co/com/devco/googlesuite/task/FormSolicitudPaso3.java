package co.com.devco.googlesuite.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.devco.googlesuite.userinterfaces.FormularioSolicitudComponent.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class FormSolicitudPaso3 implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                WaitUntil.the(EMPRESATRAB,isCurrentlyEnabled()),

                Click.on(EMPRESATRAB),
                Click.on(EMPRESATRAB2),

                Click.on(CARGO),
                Enter.theValue("DOCENTE").into(CARGO),

                Click.on(FECHAVINCULACION),
                Click.on(FECHAVINCULACION2),

                Click.on(DIRTRAB),
                Enter.theValue("CRA 45 No 65-27").into(DIRTRAB),
                Click.on(BOTONDIRTRAB),

                Click.on(CIUDADTRABAJO),
                Click.on(CIUDADTRABAJO2),


                Click.on(TELEFONOTRAB),
                Enter.theValue("3852043").into(TELEFONOTRAB),

                Click.on(EXT),
                Enter.theValue("3852043").into(EXT),

                Click.on(BOTONACEPTAR3)


        );
    }
}
