package co.com.devco.googlesuite.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.devco.googlesuite.userinterfaces.FormularioSolicitudComponent.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class FormSolicitudPaso2 implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                WaitUntil.the(APELLIDO,isCurrentlyEnabled()),
                Click.on(APELLIDO),
                Enter.theValue("RAMIREZ").into(APELLIDO),

                Click.on(NOMBRE),
                Enter.theValue("JUAN").into(NOMBRE),

                Click.on(SELCEDULA),
                Click.on(SELCEDULA2),

                Click.on(LUGEXPEDICION),
                Click.on(LUGEXPEDICION2),

                Click.on(FECHEXPEDICION),
                Click.on(FECHEXPEDICION2),

                Click.on(FECHANACIMIENTO),
                Click.on(FECHANACIMIENTO2),

                Click.on(ESTADO),
                Click.on(ESTADO2),

                Click.on(LUGNACIMIENTO),
                Click.on(LUGNACIMIENTO2),

                Click.on(ESTADOCIVIL),
                Click.on(ESTADOCIVIL2),

                Click.on(CIUDADEP),
                Click.on(CIUDADEP2),

                Click.on(TIPOVIVIENDA),
                Click.on(TIPOVIVIENDA2),

                Click.on(ESTRATO),
                Click.on(ESTRATO2),

                Click.on(DIRECCION),
                Enter.theValue("CRA 73 No 88-27").into(DIRECCION),
                Click.on(BOTONDIR),

                Click.on(BARRIO),
                Enter.theValue("VILLA CAROLINA").into(BARRIO),

                Click.on(TELEFONO),
                Enter.theValue("3681014").into(TELEFONO),

                Click.on(CELULAR),
                Enter.theValue("3012061343").into(CELULAR),

                Click.on(CORRESPONSAL),
                Click.on(CORRESPONSAL2),

                Click.on(EMAIL),
                Enter.theValue("sqa2@finsocial.co").into(EMAIL),

                Click.on(ANOSRESIDENCIA),
                Enter.theValue("5").into(ANOSRESIDENCIA),

                Click.on(MESESRESIDENCIA),
                Enter.theValue("8").into(MESESRESIDENCIA),

                Click.on(EPS),
                Enter.theValue("SURA").into(EPS),


                Click.on(ADULTO),
                Enter.theValue("2").into(ADULTO),

                Click.on(NINO),
                Enter.theValue("1").into(NINO),

                Click.on(NIVEL),
                Click.on(NIVEL2),

                Click.on(PROFESION),
                Click.on(PROFESION2),

                Click.on(BOTONACEPTAR2)

        );

    }
}
