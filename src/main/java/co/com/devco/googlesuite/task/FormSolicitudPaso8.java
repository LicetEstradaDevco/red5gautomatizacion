package co.com.devco.googlesuite.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.devco.googlesuite.userinterfaces.FormularioSolicitudComponent.BOTONACEPTAR8;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class FormSolicitudPaso8 implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(BOTONACEPTAR8,isCurrentlyEnabled()),
                Click.on(BOTONACEPTAR8)
        );
    }
}
