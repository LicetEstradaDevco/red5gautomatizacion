package co.com.devco.googlesuite.task;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;
import java.awt.event.KeyEvent;

import static co.com.devco.googlesuite.userinterfaces.LoginFinsocialComponent.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class Ingreso implements Task {

    private String usuario ;
    private String contrasena;
    private String codigo;

    public Ingreso(String usuario, String contrasena, String codigo) {
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.codigo = codigo;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                WaitUntil.the(USUARIO,isCurrentlyEnabled()),
                Click.on(USUARIO),
                Enter.theValue(this.usuario).into(USUARIO),

                Click.on(CONTRASENA),
                Enter.theValue(this.contrasena).into(CONTRASENA),
                Click.on(ACCESO),
                Enter.theValue(this.codigo).into(CODIGO),
                Click.on(ENVIAR));



    }


    public static Ingreso.IngresoBuilder usuario(String usuario) {
        return new Ingreso.IngresoBuilder(usuario);
    }

    public static class IngresoBuilder {
        private String usuario;
        private String contrasena;
        private String codigo;

        public IngresoBuilder(String usuario) {
            this.usuario = usuario;
        }

        public IngresoBuilder de(String contrasena) {
            this.contrasena = contrasena;
            return this;
        }

        public Ingreso a(String codigo) {
            this.codigo = codigo;
            return instrumented(Ingreso.class, this.usuario, this.contrasena, this.codigo);
        }
    }


}
