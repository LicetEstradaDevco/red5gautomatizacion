package co.com.devco.googlesuite.task;

import net.bytebuddy.asm.Advice;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.ScrollToBy;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;
import static co.com.devco.googlesuite.userinterfaces.CreacionComponent.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyVisible;

public class CreandoInfoCredito  implements Task {

    private String cedula ;
    private String cuota;
    private String plazo;

    public CreandoInfoCredito(String cedula, String cuota, String plazo) {
        this.cedula = cedula;
        this.cuota  = cuota;
        this.plazo = plazo;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                WaitUntil.the(CEDULA,isCurrentlyEnabled()),
                Click.on(CEDULA),
                Enter.theValue(this.cedula).into(CEDULA).thenHit(Keys.ENTER),
                Click.on(CUOTA),
                Enter.theValue(this.cuota).into(CUOTA),
                Click.on(PLAZO),
                Enter.theValue(this.plazo).into(PLAZO),
                WaitUntil.the(CREAR,isCurrentlyVisible()),
                Click.on(CREAR),
                WaitUntil.the(CONFIRMAR,isCurrentlyVisible())

        );
    }

    public static CreandoInfoCredito.CreandoInfoCreditoBuilder cedula(String cedula) {
        return new CreandoInfoCredito.CreandoInfoCreditoBuilder(cedula);
    }

    public static class CreandoInfoCreditoBuilder {
        private String cedula;
        private String cuota;
        private String plazo;

        public CreandoInfoCreditoBuilder(String cedula) {
            this.cedula = cedula;
        }
        public CreandoInfoCreditoBuilder de(String cuota) {
            this.cuota = cuota;
            return this;
        }

        public CreandoInfoCredito a(String plazo) {

            this.plazo = plazo;

            double  numero =  (Math.random() * 300000);
            System.out.println (numero);
            return instrumented(CreandoInfoCredito.class, this.cedula, this.cuota, this.plazo);
        }
    }
}
