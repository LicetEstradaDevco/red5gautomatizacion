package co.com.devco.googlesuite.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;


import static co.com.devco.googlesuite.userinterfaces.FormularioSolicitudComponent.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class FormSolicitudPaso1 implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                WaitUntil.the(CIUDAD,isCurrentlyEnabled()),
                Click.on(CIUDAD),
                Click.on(CIUDADCAMPO),
                Enter.theValue("CALI - VALLE").into(CIUDADCAMPO).thenHit(Keys.ENTER),
                Click.on(CALIFICACION),
                Enter.theValue("800").into(CALIFICACION),
                Click.on(VINCULACION),
                Click.on(VINCULACION2),
                Click.on(MONTO),
                Enter.theValue("1000000").into(MONTO).thenHit(Keys.ENTER),
                Click.on(CUOTA2),
                Enter.theValue("89000").into(CUOTA2).thenHit(Keys.ENTER),
                Click.on(PLAZO),
                Enter.theValue("98").into(PLAZO).thenHit(Keys.ENTER),
                Click.on(TASAINT),
                Enter.theValue("2").into(TASAINT).thenHit(Keys.ENTER),
                Click.on(TASAMOR),
                Enter.theValue("2").into(TASAMOR).thenHit(Keys.ENTER),
                Click.on(TASAMAXLEGVIG),
                Enter.theValue("2").into(TASAMAXLEGVIG).thenHit(Keys.ENTER),
                Click.on(CONTINUAR1)

        );

    }



}
