package co.com.devco.googlesuite.question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static co.com.devco.googlesuite.userinterfaces.CreacionComponent.CUENTA2;


public class VerificacionCreacion implements Question <String> {

    String texto;

    public VerificacionCreacion (String texto){
        this.texto = texto;
    }

    @Override
    public String answeredBy(Actor actor) {

        return Text.of(CUENTA2).viewedBy(actor).asString();
    }

    public static VerificarIngreso is (String texto){

        return new VerificarIngreso(texto);
    }




}
